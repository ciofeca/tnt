/* tnt: Hostmode Terminal for TNC
   Copyright (C) 1993-1997 by Mark Wahl
   For license details see documentation
   include file for AX25-Kernel-Interface (ax25k.h)
   created: Mark Wahl DL4YBG 97/02/16
   updated: Mark Wahl DL4YBG 97/02/24
*/

/* definitions of messages for timestamping */
#define ST_EVER 0
#define ST_MONI 1
#define ST_STAT 2

/* definitions of monitor frames to display */
#define MONI_UNPR 1
#define MONI_INFO 2
#define MONI_SUPV 4
#define MONI_CONN 8

void free_ax25k();
int alloc_ax25k();
int init_ax25k();
void init_statline();
void open_upfile();
extern void open_upfile();
void handle_tx_queue();
void status_ax25k(int always);
void ax25k_fdset();
int ax25k_receive();
void exit_ax25k();
void write_ax25k();
void command_ax25k();
