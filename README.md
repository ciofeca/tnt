## TNT - A UNIX Packet-Radio Terminal Program

This is an attempt to get TNT working again, based on TNT 1.8 version sources released on april 1999 by Mark Wahl:

https://web.archive.org/web/20010607222125/http://ma.yer.at/ham/

Version 1.0 was released on 26-Jan-1997 ([documentation](doc/tntdoc.html)).

## Compiling

Only tested under Ubuntu 20.04; requires *libncurses-dev* package already installed.

Pretty old *autoconf* from the 20th century requires a target (as *x86_64* wasn't ready yet) to create *Makefile*'s, for example:

    ./configure --host=i686-pc-linux-gnu

Currently *dpboxt* and *ax25k* features won't compile (both probably require extensive work).

Compiling (executables: *src/tnt* and *src/tntc*):

    make

Cleanup and reconfigure if serious changes:

    make clean
    rm -f config.cache config.log config.status
    rm -f include/config.h include/tntrun.h conf/tnt_setup.de conf/tnt_setup.en examples/sounds.tnt
    rm -f contrib/help contrib/info contrib/logcall
    rm -f Makefile */Makefile src/*.o src/tnt src/tntc
    ./configure --host=i686-pc-linux-gnu

