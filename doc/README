Note: This is release of TNT 1.8, originally developed by Mark Wahl.
This release is the work of several people. Take a look in ./doc/CHANGES
for more information.

---------------------------------------------------------------------
QUICK-INSTALLATION, please read carefully

To install TNT:
  1) Type "./configure --help" to see the options you may be want to
     change (for example sound-support is defaultly compiled in).
  2) Type "./configure" or "./configure --[ARGS]" to configure TNT.
     (For example "./configure --disable-sound" to compile TNT without
     sound).
  3) Type "make" to build TNT.
  4) Type "make install" to install TNT.
  5) Good Luck ;-)

NOTE: for german-users:
  Um ein deutsprachiges Setup und deutsprachige Onlinehilfen zu erhalten
  muss beim configure die Option "--disable-english" angegeben werden.

To uninstall TNT:
  1) Type "make uninstall" to uninstall TNT.
     You don't want to do this I think ;-) If you do, all files which were
     installed (and only these) will be deleted.
---------------------------------------------------------------------
Configure-Options:
  There are five options for TNT:
  a) sound
  b) ax25k
  c) dpboxt
  d) english
  e) hibaud (only supported on linux-systems)

 Defaultly only sound and english is activated.
 To disable sound: ./configure --disable-sound
 To enable ax25k:  ./configure --enable-ax25k
 To enable german: ./configure --disable-english
 To enable hibaud: ./configure --enable-hibaud
 To enable dpboxt: ./configure --enable-dpboxt
 (Note: the dpbox-terminal is broken at the moment, so compilation with
        DPBOXT will fail!
  Note2: DPBOXT will disappear in further versions, so use an old TNT
         version, or wait for a new console, I will write if I have the
         time to do so ;-)

AX25-kernel support:
If you want to use TNT with ax25-kernel support, the only thing to do, is
configuring TNT with ax25k enabled.
Please Note: you MUST have the axutils installed. TNT needs the ax25lib
(libax25.a) and the include-files (there should exists in /usr/include/ax25
for example). TNT searches for an include-dir named "ax25".
If you compiled axutils, there must be a library "libax25.a". copy this file
to /usr/lib or /usr/local/lib and run "ldconfig", so TNT can find this lib.

HIBAUD:
Enable this, if you using a baudrate >38400 to your TNC. (Only supported on
Linux).

Installation is easy: "./configure", "make" and "make install".
After the installation is finished the program "tnt_setup" will be called.
Here you can do some important settings for your tnc, boxsocket or your
callsign. If you do not want this, just leave the program.

-------------------------------------------------------------------------
IMPORTANT:
TNT can now used by more than one user. Just install TNT as normal, and
than create a dir ".tnt" in your homedirectory and put ALL the Configuration-
files in it (tnt.ini and the *.tnt files).

If you don't start TNT with a given directory explizit (with the -i option),
TNT will search "tnt.ini" (or ".tntrc") in these places:
First in your $HOME/.tnt directory, e.g.  /home/you/.tnt/tnt.ini
Then in the current directory.
After this TNT will look in the directory you installed TNT in (Note: the
path in configure.h "TNT_INSTALL_PATH" will be hardcoded into TNT).
At last TNT looks in /etc/tnt.ini

The same thing is the "tntc.ini" (or ".tntrc"),
---------------------------------------------------------------------
NEW IN THIS VERSION: READ CAREFULLY!

Hierarchical filelocations changed.
Defaultprefix-directory is: /usr/local

The executables of TNT will now stored in $prefix/bin
Setuprogram is stored in $prefix/sbin
All configurations file can be found in $prefix/share/tnt
ALL logfiles are now in /var/log
Pipes (sockets, pids) are now in /var/run. NOTE: You really should
put your dpbox-socket and tfkiss-sockets in this dir too.
/var/spool/tnt is used for writing and reading information (eg. file up-
and downloads, runprograms.
---------------------------------------------------------------------

documentation of TNT can be found in /doc-dir.
source-code is found in /src-dir.
solaris-stuff is found in /doc/solaris-dir.
examples-configuration files can be found in /examples-dir.

for information about what has changed, take a look at /doc/CHANGES

------------------------------------------------------------------------------
TNT Mailinglists

For discussing new features in further TNT-versions, to find and solve problems
there are some mailinglists. If you want to know whats going on, or if you
want to be informed of new versions, just subscribe to these list:

"tnt-devel": Here we discuss about new features, and about the further
development.
To subscribe to this list, send a mail with the word "SUBSCRIBE" in the mail-
content to:
mailto: tnt-devel-request@excelsior.kullen.rwth-aachen.de

"tnt-bugs": Here you should post all the problems you have with TNT, or bugs
and errors you have found.
To subscribe to this list, send a mail with the word "SUBSCRIBE" in the mail-
content to:
mailto: tnt-bugs-request@excelsior.kullen.rwth-aachen.de

"tnt-announce": If you want to be informed about new things about TNT and
TNT/Packet-Radio related stuff, just join this list.
To subscribe to this list, send a mail with the word "SUBSCRIBE" in the mail-
content to:
mailto: tnt-announce-request@excelsior.kullen.rwth-aachen.de

For information about using the mailserver, or to get the archives of our
discussions, send a mail with the word "HELP" in the mail-content to:
mailto: majordomo@excelsior.kullen.rwth-aachen.de

If you ever encounter a problem with the list, just mailto: wsp@gmx.de
or mailto: <list>-owner@excelsior.kullen.rwth-aachen.de, where <list> is
the name of the list you have trouble with.
------------------------------------------------------------------------------
You can find new versions of TNT here:
http://excelsior.kullen.rwth-aachen.de
ftp://excelsior.kullen.rwth-aachen.de/pub/packet_radio/tnt
http://www.bfl.at/mayer/ham
ftp://ftp.bfl.at/src/tnt

The Original-Home of TNT is here:
http://www.snafu.de/~wahlm

Bugs and problem reports to: tnt-bugs@excelsior.kullen.rwth-aachen.de
(You may want to be subscribe to the mailing-lists, see above)

NOTE: This release is no offical version of Mark Wahl (Author of TNT).
      But there will be definitly no more versions from Mark Wahl!

Contents last checked by Matthias Hensler.
(WSPse, 18. Apr. 1999)
